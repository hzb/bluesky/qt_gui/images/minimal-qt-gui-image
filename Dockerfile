# Bluesky Base Dockerfile

##### shared environment stage #################################################

# Use Ubuntu 20.04 as the base image.
#FROM ubuntu:20.04 AS base
FROM python:3.11.4-slim-bullseye
ENV TZ=Europe/Berlin

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    tzdata \
    python3 \
    python3-pip \
    re2c \
    rsync \
    ssh-client \
    nano
    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev 
    
RUN apt install -y libzbar-dev
RUN pip install --upgrade pip
RUN python3 -m pip install PyQt5==5.15.11 PyQt5-Qt5==5.15.15 PyQt5_sip==12.15.0

# Install Python packages required for Bluesky and scientific computing.
RUN python3 -m pip install bluesky==1.12.0 ophyd==1.9.0 matplotlib==3.9.2 ipython==8.27.0 pyepics==3.5.7
# RUN python3 -m pip install Levenshtein lmfit jsonschema==3.2 scikit-image
RUN python3 -m pip install Levenshtein lmfit==1.3.2 jsonschema==3.2 scikit-image==0.24.0
# Install packages for publisher
RUN python3 -m pip install apstools==1.6.20 zmq==0.0.0
# install bluesky_queueserver_api
RUN python3 -m pip install bluesky_queueserver_api==0.0.11
#install qtconsole
RUN python3 -m pip install qtconsole==5.6.0


# Clone the 'beamline_test' Git repository into the 'bluesky' directory.
RUN mkdir /opt/bluesky 
# Clone bluesky-emil-gui
RUN cd /opt/bluesky && git clone --depth 1 --branch v1.0.3 https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/source/bluesky-emil-gui.git
# Clone bluesky-widgets
RUN cd /opt/bluesky && git clone --depth 1  --branch v1.0.0 https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/source/bluesky-widgets.git
# Clone bessy-bluesky-widgets
RUN cd /opt/bluesky && git clone --depth 1 --branch v1.10.2 https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/source/bessy-bluesky-widgets.git /opt/bluesky/bessyii_bluesky_widgets
# Clone bluesky-live
RUN cd /opt/bluesky && git clone --depth 1 --branch v1.0.0 https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/source/bluesky-live.git

# Install the packages downloaded in bluesky
RUN cd /opt/bluesky/bluesky-widgets && python3 -m pip install -e .
RUN cd /opt/bluesky/bessyii_bluesky_widgets && python3 -m pip install -e .
RUN cd /opt/bluesky/bluesky-emil-gui && python3 -m pip install -e .
RUN cd /opt/bluesky/bluesky-live && python3 -m pip install -e .


RUN python3 -m pip install --force-reinstall qtpy
RUN python3 -m pip install --force-reinstall PyQt5==5.15.11
