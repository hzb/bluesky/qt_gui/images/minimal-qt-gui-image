#! /bin/sh
xhost +
docker run -it --rm --env DISPLAY=${DISPLAY} --network host -v /tmp/.X11-unix:/tmp/.X11-unix  -v /home/myspot/bluesky/bluesky_gui/bessy-bluesky-widgets:/opt/bluesky/bessyii_bluesky_widgets -v $(pwd)/../bluesky-emil-gui:/opt/bluesky/bluesky-emil-gui -v $(pwd)/../startup_scripts:/opt/bluesky/startup registry.hzdr.de/hzb/bluesky/qt_gui/images/minimal-qt-gui-image:bugfix_plan_creator /opt/bluesky/startup/dev_start.sh 

