c = get_config()

# Enable history saving
c.HistoryManager.enabled = True

# Set the history file path
c.HistoryManager.hist_file = '/opt/bluesky/ipython/profile_root/history.sqlite'