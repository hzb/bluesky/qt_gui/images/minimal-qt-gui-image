# minimal-qt-gui-image container 

This repository is used to configure an environment for running a minimal version of the QTGui to control a bluesky queueserver.

## How to run the demo

1. Install and run a bluesky queueserver. You can follow these instructions: https://blueskyproject.io/bluesky-queueserver/installation.html

2. To use the full functionality of the GUI you might want to setup your ipython profile using plan annotation. See https://blueskyproject.io/bluesky-queueserver/plan_annotation.html for reference. The following repository contains an example profile with simulated devices: https://codebase.helmholtz.cloud/hzb/bluesky/ipython_profile

You can then start the queueserer like this: `start-re-manager --startup-dir=[PATH_TO_THE_PROFILE]/startup --keep-re`

For example if you are inside your project folder where you copied the profile to: `start-re-manager --startup-dir=./ipython_profile/startup --keep-re`

3. Install and run a service for plotting over 0mq. This repository for example has a container that plots using the best effort callback of bluesky: https://codebase.helmholtz.cloud/hzb/bluesky/core/images/bec_plotting.


4. To add a callback via 0mq you need to run a 0MQ buffer somewhere. The easiest way is in another shell with the command (assuming you're in a python environment with bluesky and 0MQ installed): `bluesky-0MQ-proxy 5577 5578`

You can also start a container like that:
`docker run -d --rm --name bluesky_zmq --network host registry.hzdr.de/hzb/bluesky/core/images/zmq-proxy:1-0-0 bluesky-0MQ-proxy 5577 5578`

5. configure X11 forwarding by issuing the command `xhost +local:docker`

6. run the following to start the GUI connecting to a specific qserver. Note the title is specified and a directory is mounted from the host for storing batches.

`docker run -it --rm --env BATCH_SAVE_DIR="/opt/batches" --env QSERVER_ZMQ_CONTROL_ADDRESS=tcp://localhost:60615 --env QSERVER_ZMQ_INFO_ADDRESS=tcp://localhost:60625 --env DISPLAY=${DISPLAY} --network host -v /batches:/opt/batches -v /tmp/.X11-unix:/tmp/.X11-unix registry.hzdr.de/hzb/bluesky/qt_gui/images/minimal-qt-gui-image:v1.0.4 bluesky-emil-gui  --title "My Awesome Beamline" --zmq localhost:5578`



## Code Structure of GUI

In the image below you can see the structure of the packages 'bluesky_emil_gui' and 'bessyii_bluesky_widgets' in this project. You can see a distinction between widgets and models. A widget in this project means a description of a pyqt element that describes a layout of buttons and other elements and attaches them to functions that can interact with certain events. A model provides the data model for the GUI with events and functions the widget can subscribe to. The different models are bundled and customized in 'bluesky_emil_gui' into the viewermodel that is then passed to any widget that needs to interact with for example the queueserer. This way all widgets can subscribe to the events of the queueserer at the same time and information that is changed in one widget is also changed in every other that is subscribed to the data flow.

![Example](Code_Structure_Bluesky_GUI.drawio.png)